import flask

from http import HTTPStatus

from . import utils


def test_get_logout_success(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    user_id = utils.user_factory(db_conn, user["username"], user["password"], "user")

    with app_client.session_transaction() as sess:
        sess["user.id"] = user_id

    rv = app_client.get("/logout", follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK

    assert flask.session.get("user.id") is None


def test_get_logout_unauthorized(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    rv = app_client.get("/logout", follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED
