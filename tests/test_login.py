import flask

from http import HTTPStatus

from . import utils


def test_get_login_success(app_client):
    rv = app_client.get("/login")
    assert rv.status_code == HTTPStatus.OK
    assert rv.mimetype == "text/html"
    assert b"Login" in rv.data
    assert b"Username" in rv.data
    assert b"Password" in rv.data
    assert b"Connect" in rv.data


def test_post_login_user_success(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    params = {"username": user["username"], "password": user["password"]}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK

    assert isinstance(flask.session.get("user.id"), int)


def test_post_login_admin_success(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "admin")

    params = {"username": user["username"], "password": user["password"]}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK

    assert isinstance(flask.session.get("user.id"), int)


def test_post_login_unknown_role(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "unknown-role")

    params = {"username": user["username"], "password": user["password"]}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.FORBIDDEN


def test_post_login_unknown_user(app_client, db_conn, faker):
    params = {"username": faker.user_name(), "password": faker.password()}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_post_login_null_username(app_client, db_conn, faker):
    params = {"username": None, "password": faker.password()}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_post_login_wrong_username(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    params = {"username": faker.user_name(), "password": user["password"]}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_post_login_null_password(app_client, db_conn, faker):
    params = {"username": None, "password": faker.password()}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_post_login_wrong_password(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    params = {"username": user["username"], "password": faker.password()}

    rv = app_client.post("/login", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.UNAUTHORIZED
