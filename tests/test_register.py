from http import HTTPStatus

from . import utils


def test_get_register_success(app_client):
    rv = app_client.get("/register")
    assert rv.status_code == HTTPStatus.OK
    assert rv.mimetype == "text/html"
    assert b"Register" in rv.data
    assert b"Username" in rv.data
    assert b"Password" in rv.data
    assert b"Create Account" in rv.data


def test_post_register_success(app_client, db_conn, faker):
    params = {"username": faker.user_name(), "password": faker.password()}

    rv = app_client.post("/register", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK

    with db_conn:
        r = db_conn.execute("SELECT * FROM users")
        users = r.fetchall()
        assert len(users) == 1

        u = users[0]
        assert u["username"] == params["username"]
        assert u["password_hash"] != params["password"]
        assert u["role"] == "user"


def test_post_register_existing_username(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    params = {"username": user["username"], "password": faker.password()}

    rv = app_client.post("/register", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.BAD_REQUEST

    with db_conn:
        r = db_conn.execute("SELECT * FROM users")
        users = r.fetchall()
        assert len(users) == 1


def test_post_register_null_username(app_client, db_conn, faker):
    params = {"username": None, "password": faker.password()}

    rv = app_client.post("/register", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.BAD_REQUEST

    with db_conn:
        r = db_conn.execute("SELECT * FROM users")
        users = r.fetchall()
        assert len(users) == 0


def test_post_register_null_password(app_client, db_conn, faker):
    params = {"username": faker.user_name(), "password": None}

    rv = app_client.post("/register", data=params, follow_redirects=True)
    assert rv.status_code == HTTPStatus.BAD_REQUEST

    with db_conn:
        r = db_conn.execute("SELECT * FROM users")
        users = r.fetchall()
        assert len(users) == 0
