import os
import tempfile
import pytest


os.environ["FLASK_ENV"] = "testing"
os.environ["SECRET_KEY"] = "secret"


@pytest.fixture(scope="session")
def faker():
    from faker import Faker

    Faker.seed(1234)
    faker_instance = Faker()

    return faker_instance


@pytest.fixture(scope="session")
def db_conn():
    from user_session_example import db

    db_fd, db_uri = tempfile.mkstemp()
    os.environ["DATABASE_URI"] = db_uri

    conn = db.create_connection(db_uri)
    yield conn
    db.close_connection(conn)

    os.close(db_fd)
    os.unlink(db_uri)


@pytest.fixture(scope="function")
def db_schema(db_conn):
    from user_session_example import models

    models.create_db_schema(db_conn)
    yield db_conn
    models.drop_db_schema(db_conn)


@pytest.fixture(scope="function")
def app_client(db_schema):
    from user_session_example import app

    with app.app.test_client() as client:
        yield client
