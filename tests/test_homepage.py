from http import HTTPStatus


def test_get_homepage_success(app_client):
    rv = app_client.get("/")
    assert rv.status_code == HTTPStatus.OK
    assert rv.mimetype == "text/html"
    assert b"Welcome" in rv.data
    assert b"Register" in rv.data
    assert b"Login" in rv.data
