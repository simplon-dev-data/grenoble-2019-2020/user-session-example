from http import HTTPStatus

from . import utils


def test_get_dashboard_user_success(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    user_id = utils.user_factory(db_conn, user["username"], user["password"], "user")

    with app_client.session_transaction() as sess:
        sess["user.id"] = user_id

    rv = app_client.get("/dashboard/user")
    assert rv.status_code == HTTPStatus.OK

    assert rv.mimetype == "text/html"
    assert b"User" in rv.data


def test_get_dashboard_user_unauthorized(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "user")

    rv = app_client.get("/dashboard/user")
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_get_dashboard_user_forbidden(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    user_id = utils.user_factory(db_conn, user["username"], user["password"], "admin")

    with app_client.session_transaction() as sess:
        sess["user.id"] = user_id

    rv = app_client.get("/dashboard/user")
    assert rv.status_code == HTTPStatus.FORBIDDEN


def test_get_dashboard_admin_success(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    user_id = utils.user_factory(db_conn, user["username"], user["password"], "admin")

    with app_client.session_transaction() as sess:
        sess["user.id"] = user_id

    rv = app_client.get("/dashboard/admin")
    assert rv.status_code == HTTPStatus.OK

    assert rv.mimetype == "text/html"
    assert b"Admin" in rv.data


def test_get_dashboard_admin_unauthorized(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    utils.user_factory(db_conn, user["username"], user["password"], "admin")

    rv = app_client.get("/dashboard/admin")
    assert rv.status_code == HTTPStatus.UNAUTHORIZED


def test_get_dashboard_admin_forbidden(app_client, db_conn, faker):
    user = {"username": faker.user_name(), "password": faker.password()}
    user_id = utils.user_factory(db_conn, user["username"], user["password"], "user")

    with app_client.session_transaction() as sess:
        sess["user.id"] = user_id

    rv = app_client.get("/dashboard/admin")
    assert rv.status_code == HTTPStatus.FORBIDDEN
