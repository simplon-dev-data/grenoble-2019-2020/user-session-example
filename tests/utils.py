def user_factory(conn, username, password, role):
    from werkzeug.security import generate_password_hash

    query = """
        INSERT INTO users (username, password_hash, role)
        VALUES (:username, :password_hash, :role)
    """

    params = {
        "username": username,
        "password_hash": generate_password_hash(password),
        "role": role,
    }

    user_id = None
    with conn:
        cur = conn.execute(query, params)
        user_id = cur.lastrowid

    return user_id
