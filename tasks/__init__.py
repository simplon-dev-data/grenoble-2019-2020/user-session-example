from invoke import task


app_path = "user_session_example"
tests_path = "tests"


@task
def lint(ctx):
    ctx.run(f"flake8 {app_path} {tests_path}", pty=True)


@task
def test(ctx):
    ctx.run(f"py.test -v --cov={app_path} --cov-branch --cov-report=term-missing {tests_path}", pty=True)


@task(lint, test)
def qa(ctx):
    pass


@task
def reformat(ctx):
    ctx.run(f"black {app_path} {tests_path}", pty=True)
