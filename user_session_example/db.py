import sqlite3


def create_connection(db_uri):
    db_conn = sqlite3.connect(db_uri)
    db_conn.row_factory = sqlite3.Row
    return db_conn


def close_connection(conn):
    conn.close()
