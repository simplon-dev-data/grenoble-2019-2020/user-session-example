import sqlite3

from enum import Enum
from werkzeug.security import check_password_hash, generate_password_hash


class UserRoleEnum(str, Enum):
    user = "user"
    admin = "admin"


user_roles = [r.value for r in UserRoleEnum]


def create_db_schema(conn):
    with conn:
        conn.execute(
            """
            CREATE TABLE users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username VARCHAR(255) UNIQUE NOT NULL,
                password_hash VARCHAR(255) UNIQUE NOT NULL,
                role TEXT
            )
        """
        )


def drop_db_schema(conn):
    with conn:
        conn.execute("""DROP TABLE users""")


def create_user(conn, username, password, role):
    if not username or not password or role not in user_roles:
        raise ValueError

    query = """
        INSERT INTO users (username, password_hash, role)
        VALUES (:username, :password_hash, :role)
    """

    password_hash = generate_password_hash(password)
    values = {"username": username, "password_hash": password_hash, "role": role}

    try:
        with conn:
            conn.execute(query, values)
    except sqlite3.Error:
        raise ValueError


def get_user(conn, user_id):
    user = None

    query = """
        SELECT * FROM users
        WHERE id = :id
    """

    values = {"id": user_id}

    try:
        with conn:
            cur = conn.execute(query, values)
            user = cur.fetchone()
    except sqlite3.Error:
        raise ValueError

    return user


def authenticate_user(conn, username, password):
    if not username or not password:
        raise ValueError

    user = None

    query = """
        SELECT * FROM users
        WHERE username = :username
    """

    values = {"username": username}

    try:
        with conn:
            cur = conn.execute(query, values)
            user = cur.fetchone()
    except sqlite3.Error:
        raise ValueError

    if user and check_password_hash(user["password_hash"], password):
        return user

    raise ValueError
