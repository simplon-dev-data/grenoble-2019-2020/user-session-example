import functools
import os
import click

from http import HTTPStatus
from flask import Flask, abort, g, redirect, render_template, request, session, url_for

from . import models


secret_key = os.environ.get("SECRET_KEY")
db_uri = os.environ.get("DATABASE_URI")


def get_db_conn():
    from . import db

    if "conn" not in g:
        g.conn = db.create_connection(db_uri)
    return g.conn


def close_db_conn(e=None):
    from . import db

    conn = g.pop("conn", None)
    if conn:
        db.close_connection(conn)


def load_logged_in_user():
    user_id = session.get("user.id")

    if user_id is None:
        g.user = None
    else:
        conn = get_db_conn()
        g.user = models.get_user(conn, user_id)


app = Flask(__name__)
app.secret_key = secret_key
app.before_request(load_logged_in_user)
app.teardown_appcontext(close_db_conn)


@app.cli.command("init-db")
def init_db_command():
    conn = get_db_conn()
    models.create_db_schema(conn)
    click.echo("Initialized the database.")


@app.cli.command("drop-db")
def drop_db_command():
    conn = get_db_conn()
    models.drop_db_schema(conn)
    click.echo("Dropped the database.")


@app.cli.command("create-user")
@click.argument("username")
@click.argument("password")
@click.argument("role")
def create_user_command(username, password, role):
    conn = get_db_conn()
    models.create_user(conn, username, password, role)
    click.echo(f"User {username} created with role {role}")


@app.cli.command("generate-secret-key")
@click.argument("nbytes", default=32)
def generate_secret_command(nbytes):
    import secrets

    new_secret_key = secrets.token_hex(nbytes)
    click.echo(f"New secret key: {new_secret_key}")


@app.cli.command("decode-session-cookie")
@click.argument("session_cookie")
def decode_session_cookie_command(session_cookie):
    from flask.sessions import SecureCookieSessionInterface

    ssci = SecureCookieSessionInterface()
    serializer = ssci.get_signing_serializer(app)
    session_cookie_values = serializer.loads(session_cookie)

    click.echo(f"Session cookie values: {session_cookie_values}")


@app.cli.command("fake-session-cookie")
@click.argument("session_cookie")
@click.argument("new_values_json")
def fake_session_cookie_command(session_cookie, new_values_json):
    import base64

    _, salt, signature = session_cookie.split(".")
    new_payload = base64.urlsafe_b64encode(new_values_json.encode("utf-8"))
    new_session_cookie = ".".join((new_payload.decode(), salt, signature))

    click.echo(f"Faker session cookie values: {new_session_cookie}")


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            abort(HTTPStatus.UNAUTHORIZED)
        return view(**kwargs)

    return wrapped_view


def role_required(role):
    def role_required_decorator(view):
        @functools.wraps(view)
        def wrapped_view(**kwargs):
            if g.user is not None and g.user["role"] == role:
                return view(**kwargs)
            abort(HTTPStatus.FORBIDDEN)

        return wrapped_view

    return role_required_decorator


@app.route("/", methods=["GET"])
def get_homepage():
    return render_template("homepage.html")


@app.route("/register", methods=["GET"])
def get_register():
    return render_template("register.html")


@app.route("/register", methods=["POST"])
def post_register():
    conn = get_db_conn()

    username = request.form.get("username")
    password = request.form.get("password")

    try:
        models.create_user(conn=conn, username=username, password=password, role="user")
    except ValueError:
        abort(HTTPStatus.BAD_REQUEST)

    return redirect(url_for("get_login"))


@app.route("/login", methods=["GET"])
def get_login():
    return render_template("login.html")


@app.route("/login", methods=["POST"])
def post_login():
    conn = get_db_conn()

    username = request.form.get("username")
    password = request.form.get("password")

    try:
        user = models.authenticate_user(conn=conn, username=username, password=password)
    except ValueError:
        abort(HTTPStatus.UNAUTHORIZED)

    user_id = user["id"]
    user_role = user["role"]
    session.clear()

    response = None
    if user_role == models.UserRoleEnum.user:
        response = redirect(url_for("get_dashboard_user"))
    elif user_role == models.UserRoleEnum.admin:
        response = redirect(url_for("get_dashboard_admin"))
    else:
        abort(HTTPStatus.FORBIDDEN)

    session["user.id"] = user_id
    return response


@app.route("/logout", methods=["GET"])
@login_required
def get_logout():
    session.clear()
    return redirect(url_for("get_login"))


@app.route("/dashboard/user", methods=["GET"])
@login_required
@role_required("user")
def get_dashboard_user():
    return render_template("dashboard_user.html", user=g.user)


@app.route("/dashboard/admin", methods=["GET"])
@login_required
@role_required("admin")
def get_dashboard_admin():
    return render_template("dashboard_admin.html", user=g.user)
