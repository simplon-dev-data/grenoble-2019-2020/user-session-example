# User Session Example

> A simple webapp using Flask and SQLite demonstrating user sessions and role-based access control

This webapp is built around Flask and SQLite to provide a simple access-controlled
user dashboard, with dynamic restriction depending on the connected user role.

## Usage

Setup the project:

1. `pipenv install -d`
2. `cp .example.env .env`
3. `pipenv shell`
4. `inv qa`

Setup the database:

1. `flask init-db`
2. `flask create-user admin test admin`
3. `flask create-user john doe user`
4. `flask run`
5. Open [http://localhost:5000](http://localhost:5000)

Then you can try the following:

1. [Create a new user account](http://localhost:5000/register)
2. [Connect to the new user account](http://localhost:5000/login)
3. [Try to access the admin dashboard](http://localhost:5000/dashboard/admin)
4. [Disconnect from the user account](http://localhost:5000/logout)
5. [Connect to an admin account](http://localhost:5000/login)
6. [Try to access the user dashboard](http://localhost:5000/dashboard/user)
7. Open your browser developer tools and inspect the cookies and see if you can change your identity! ;)

Try hacking the session cookie:

1. Decode the existing session cookie:
   `flask decode-session-cookie <session-cookie-value>`
2. Observe that you can see the actual user identification
3. Generate a fake session cookie, replacing only the user identification:
   `flask fake-session-cookie <session-cookie-value> '{"user.id": 2}'`
4. Check if the new session cookie can be decoded (should raise an error):
   `flask decode-session-cookie <new-session-cookie-value>`
5. Open [http://localhost:5000](http://localhost:5000)
6. Change the `session` cookie value in storage with the new fake value
7. [Try to access the user dashboard](http://localhost:5000/dashboard/user)

## Documentation

- [Flask](https://flask.palletsprojects.com)
- [Jinja2](https://jinja.palletsprojects.com)
- [SQLite3](https://docs.python.org/3/library/sqlite3.html)
